//sequelize, type

module.exports = (sequelize, type) =>{

  const Copie = sequelize.define('copie', {
    id: {type: type.INTEGER, primaryKey:true, autoIncrement: true},
    number: {type: type.INTEGER, notNull:true},
    format: type.INTEGER,
    movieId: type.INTEGER,
    estatus: type.INTEGER
  });

return Copie;

};
