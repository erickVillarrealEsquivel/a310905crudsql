module.exports = (sequelize, type) => {
  const Movie = sequelize.define('movie', {
    id: {type: type.INTEGER, primaryKey:true, autoIncrement: true},
    title: type.STRING,
    directorId: type.INTEGER,
    genreId: type.INTEGER
  });

  return Movie;
};
