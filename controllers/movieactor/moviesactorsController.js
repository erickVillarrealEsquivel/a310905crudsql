const express = require('express');
const { MovieActor } = require('../../sequelize');

function list(req, res, next){
  Actor.findAll({}).then(objects => res.json(objects));
}

function index(req, res, next){
  let id = req.params.id;
  MovieActor.findByPk(id, {}).then(object => res.json(object));
}

function create(req, res, next){
  let movieactor = new Object;
  movieactor.movieId = req.body.movieId;
  movieactor.actorId = req.body.actorId;
  MovieActor.create(movieactor).then(movieactor => res.json(movieactor));
}

function update(req, res, next){
  let id = req.params.id;
  MovieActor.findByPk(id, {}).then((object) =>{
    object.name = req.body.movieId ? req.body.movieId : object.movieId;
    object.actorId = req.body.actorId ? req.body.actorId : object.actorId;
    object.update({'movieId':object.movieId, 'actorId':object.actorId})
    .then( object => res.json(object));
  });
}

function destroy(req, res, next){
  let id = req.params.id;
  MovieActor.destroy({ where: {id: id}}).then( object => res.json(object));
}

module.exports = {
  index, list, create, update, destroy
}
