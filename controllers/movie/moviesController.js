const express = require('express');
const { Movie } = require('../../sequelize');

function list(req, res, next){
  Movie.findAll({}).then(objects => res.json(objects));
}

function index(req, res, next){
  let id = req.params.id;
  Movie.findByPk(id, {}).then(object => res.json(object));
}

function create(req, res, next){
  let movie = new Object;
  movie.title = req.body.title;
  movie.directorId = req.body.directorId;
  movie.genreId = req.body.genreId;
  Movie.create(movie).then(movie => res.json(movie));
}

function update(req, res, next){
  let id = req.params.id;
  Movie.findByPk(id, {}).then((object) =>{
    object.title = req.body.title ? req.body.title : object.title;
    object.directorId = req.body.directorId ? req.body.directorId : object.directorId;
    object.genreId = req.body.genreId ? req.body.genreId : object.genreId;
    object.update({'title':object.title, 'directorId':object.directorId, 'genreId':object.genreId})
    .then( object => res.json(object));
  });
}

function destroy(req, res, next){
  let id = req.params.id;
  Movie.destroy({ where: {id: id}}).then( object => res.json(object));
}

module.exports = {
  index, list, create, update, destroy
}
