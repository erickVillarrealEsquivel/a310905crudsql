const express = require('express');
const { Copie } = require('../../sequelize');

function list(req, res, next){
  Copie.findAll({}).then(objects => res.json(objects));
}

function index(req, res, next){
  let id = req.params.id;
  Copie.findByPk(id, {}).then(object => res.json(object));
}

function create(req, res, next){
  let copie = new Object;
  copie.number = req.body.number;
  copie.format = req.body.format;
  copie.movieId = req.body.movieId;
  copie.estatus = req.body.estatus;
  Copie.create(copie).then(copie => res.json(copie));
}

function update(req, res, next){
  let id = req.params.id;
  Copie.findByPk(id, {}).then((object) =>{
    object.number = req.body.number ? req.body.number : object.number;
    object.format = req.body.format ? req.body.format : object.format;
    object.movieId = req.body.movieId ? req.body.movieId : object.movieId;
    object.estatus = req.body.estatus ? req.body.estatus : object.estatus;
    object.update({'number':object.number, 'format':object.format,'movieId':object.movieId,'estatus':object.estatus})
    .then( object => res.json(object));
  });
}

function destroy(req, res, next){
  let id = req.params.id;
  Copie.destroy({ where: {id: id}}).then( object => res.json(object));
}

module.exports = {
  index, list, create, update, destroy
}
