const Sequelize = require('sequelize');
const log4js = require('log4js');
const logger = log4js.getLogger();

const actorModel = require('./models/actor');
const genreModel = require('./models/genre');
const movieModel = require('./models/movie');
const moviesActorsModel = require('./models/moviesactors');
const directorModel = require('./models/director');
const copieModel = require('./models/copie');
const bookingModel = require('./models/booking');
const memberModel = require('./models/member');

logger.level = 'debug';


const sequelize = new Sequelize('video-club', 'root','secret', {
  host:'localhost',
  dialect:'mysql'
});

const Actor = actorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const MoviesActors = moviesActorsModel(sequelize, Sequelize);
const Director = directorModel(sequelize, Sequelize);
const Copie = copieModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);

Genre.hasMany(Movie, {as: 'movies'});
Movie.belongsTo(Genre, {as: 'genre'});

MoviesActors.belongsTo(Movie, {foreignKey: 'movieId'});
MoviesActors.belongsTo(Actor, {foreignKey: 'actorId'});

Movie.belongsToMany(Actor, {
  through: 'moviesActors',
  foreignKey: 'actorId',
  as: 'actors'
});

Actor.belongsToMany(Movie, {
  through: 'moviesActors',
  foreignKey: 'movieId',
  as: 'movies'
});

Director.hasMany(Movie, {as: 'movies'});
Movie.belongsTo(Director, {as: 'director'});

Movie.hasMany(Copie, {as: 'copie'});
Copie.belongsTo(Movie, {as: 'movie'});

Booking.belongsTo(Copie, {foreignKey: 'copieId'});
Booking.belongsTo(Member, {foreignKey: 'memberId'});

Movie.belongsToMany(Member, {
  through: 'booking',
  foreignKey: 'memberId',
  as: 'member'
});

Actor.belongsToMany(Copie, {
  through: 'booking',
  foreignKey: 'copieId',
  as: 'copie'
});

sequelize.sync({
  force: true
}).then(()=>{
  logger.info("Database created !!!!");
});

module.exports = {
  Actor, Genre, Movie
};
